package ru.t1.chubarov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint{

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectClearResponse clearProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectClearRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectCreateResponse createProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectCreateRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectGetByIdResponse getProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectGetByIdRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectShowResponse listProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectShowRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectRemoveByIdRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectTaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectTaskBindToProjectRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectTaskUnbindToProjectResponse unbindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectTaskUnbindToProjectRequest request);

    @NotNull
    @SneakyThrows
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ProjectUpdateByIdRequest request);

}
