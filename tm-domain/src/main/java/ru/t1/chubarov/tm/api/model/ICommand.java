package ru.t1.chubarov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;

import java.io.IOException;

public interface ICommand {

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    void execute() throws AbstractException, IOException;

}
