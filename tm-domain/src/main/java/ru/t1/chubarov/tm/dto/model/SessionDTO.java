package ru.t1.chubarov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractUserOwnerModelDTO {

    @NotNull
    @Column(nullable = false, name = "date")
    private Date date = new Date();

    @NotNull
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Id
    @NotNull
    @Column(nullable = false, name = "id")
    private String id = UUID.randomUUID().toString();

}
