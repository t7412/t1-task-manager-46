package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IUserEndpoint;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.dto.response.UserLoginResponse;
import ru.t1.chubarov.tm.marker.SoapCategory;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.service.TokenService;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @Nullable
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Nullable
    private String token;

    @Before
    public void initTest() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        @Nullable final UserRemoveRequest requestRemove = new UserRemoveRequest(token);
        requestRemove.setLogin("cat");
        userEndpoint.removeUser(requestRemove);
        authEndpoint.logout(new UserLogoutRequest(token));
    }

    @Test
    public void testUserRegistry() {
        token = authEndpoint.login(new UserLoginRequest("user", "user")).getToken();
        tokenService.setToken(token);
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin("cat");
        request.setPassword("cat");
        request.setEmail("cat@test.org");
        @NotNull final UserDTO user = userEndpoint.registryUser(request).getUser();
        Assert.assertEquals("cat", user.getLogin());
        Assert.assertEquals("cat@test.org", user.getEmail());
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(request));
    }

    @Test
    public void testUserLock() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        @NotNull final UserLockRequest request = new UserLockRequest(token);
        request.setLogin("user");
        userEndpoint.lockUser(request);
        authEndpoint.logout(new UserLogoutRequest(token));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("user", "user")).getToken());
    }

    @Test
    public void testUserUnlock() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        @NotNull final UserLockRequest request = new UserLockRequest(token);
        request.setLogin("user");
        userEndpoint.lockUser(request);
        authEndpoint.logout(new UserLogoutRequest(token));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("user", "user")).getToken());

        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(token);
        requestUnlock.setLogin("user");
        userEndpoint.unlockUser(requestUnlock);
        authEndpoint.logout(new UserLogoutRequest(token));
        token = authEndpoint.login(new UserLoginRequest("user", "user")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
    }

    @Test
    public void testUserRemove() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);

        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin("cat");
        request.setPassword("cat");
        request.setEmail("cat@test.org");
        @NotNull final UserDTO user = userEndpoint.registryUser(request).getUser();
        Assert.assertEquals("cat", user.getLogin());
        Assert.assertEquals("cat@test.org", user.getEmail());
        authEndpoint.logout(new UserLogoutRequest(token));
        token = authEndpoint.login(new UserLoginRequest("cat", "cat")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
        authEndpoint.logout(new UserLogoutRequest(token));
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        tokenService.setToken(token);
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(token);
        requestRemove.setLogin("cat");
        Assert.assertEquals("cat", userEndpoint.removeUser(requestRemove).getUser().getLogin());

        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("cat", "cat")));
    }

    @Test
    public void testUserChangePassword() {
        @NotNull final UserRegistryRequest requestReg = new UserRegistryRequest();
        requestReg.setLogin("cat");
        requestReg.setPassword("cat");
        requestReg.setEmail("cat@test.org");
        @NotNull final UserDTO userReg = userEndpoint.registryUser(requestReg).getUser();

        token = authEndpoint.login(new UserLoginRequest("cat", "cat")).getToken();
        tokenService.setToken(token);

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword("new_password");
        @NotNull final UserDTO user = userEndpoint.changeUserPassword(request).getUser();
        Assert.assertEquals("cat", user.getLogin());
        Assert.assertEquals("cat@test.org", user.getEmail());
        authEndpoint.logout(new UserLogoutRequest(token));

        @Nullable final UserLoginResponse responseLogin = authEndpoint.login(new UserLoginRequest("cat", "new_password"));
        token = authEndpoint.login(new UserLoginRequest("cat", "new_password")).getToken();
        tokenService.setToken(token);
        Assert.assertNotNull(token);
        authEndpoint.profile(new UserProfileRequest(token)).getUser().getLogin();
        @NotNull final UserDTO userLogin = authEndpoint.profile(new UserProfileRequest(token)).getUser();
        Assert.assertNotNull(userLogin);
        Assert.assertEquals("cat", userLogin.getLogin());
        @NotNull final UserRemoveRequest requestRemove = new UserRemoveRequest(token);
        requestRemove.setLogin("cat");
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(requestRemove).getUser());
    }

    @Test
    public void testUserUpdateProfile() {
        token = authEndpoint.login(new UserLoginRequest("user", "user")).getToken();
        tokenService.setToken(token);

        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setFirstName("Peter");
        request.setLastName("Invanov");
        request.setMiddleName("Vasilievich");
        @NotNull final UserDTO user = userEndpoint.updateUserProfile(request).getUser();
        Assert.assertEquals("Invanov", user.getLastName());
        Assert.assertEquals("Peter", user.getFirstName());
        Assert.assertEquals("Vasilievich", user.getMiddleName());
    }

}
