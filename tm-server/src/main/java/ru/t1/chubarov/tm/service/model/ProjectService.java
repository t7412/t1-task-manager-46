package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IProjectModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.DescriptionEmptyException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    private final IUserService userService;

    public ProjectService(@NotNull final IConnectionService connectionService, IUserService userService) {
        this.connectionService = connectionService;
        this.userService = userService;
    }

    @Override
    public void create(@Nullable final String userId, @NotNull final String name) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project(name, Status.NOT_STARTED);
        project.setName(name);
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        project.setUser(user);
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project(name, Status.NOT_STARTED);
        project.setName(name);
        project.setDescription(description);
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        project.setUser(user);
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable final Project model) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (model == null) throw new ModelNotFoundException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addByUserId(@Nullable final String userId, @Nullable final Project model) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        model.setUser(user);
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(@Nullable final String userId, @Nullable final String id, @NotNull final String name, @NotNull final String description) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        @NotNull final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            return repository.findAllByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull String userId, @Nullable String id) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            @Nullable final Project model = repository.findOneByIdByUser(userId, id);
            if (model == null) throw new ModelNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Project model) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project model = new Project();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize(@Nullable final String userId) throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            return repository.getSizeByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<Project> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull final Project project : models) {
            add(project);
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try  {
            @NotNull final IProjectModelRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
