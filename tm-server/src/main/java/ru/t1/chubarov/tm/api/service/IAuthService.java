package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.dto.model.SessionDTO;
import ru.t1.chubarov.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException, Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable SessionDTO session) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@NotNull SessionDTO session);

}
