package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.dto.model.AbstractUserOwnerModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModelDTO> extends IUserOwnerRepository<M>, IService<M>{

    void removeAll(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    M add(@Nullable String userId, @Nullable  M model) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws Exception;

    @NotNull
    M remove(@NotNull String userId, @Nullable M model) throws Exception;

    @NotNull
    M findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @NotNull
    M findOneByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

}
