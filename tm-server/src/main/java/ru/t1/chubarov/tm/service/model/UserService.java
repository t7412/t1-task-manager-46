package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.IUserModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.api.service.model.IProjectService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.UserRepository;
import ru.t1.chubarov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;
    @NotNull
    private final IProjectService projectService;
    @NotNull
    private final ITaskDtoService taskService;

    public UserService(@NotNull final IProjectService projectService,
                       @NotNull final ITaskDtoService taskService,
                       @NotNull final IPropertyService propertyService,
                       @NotNull final IConnectionService connectionService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException(login, email);
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public User findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final User model = repository.findOneById(id);
            if (model == null) throw new UserNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public void removeOne(@Nullable final User model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            @Nullable final String userId = model.getId();
            entityManager.getTransaction().begin();
            repository.remove(model);
            entityManager.getTransaction().commit();
            taskService.removeAll(userId);
            projectService.removeAll(userId);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        removeOne(user);
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException("login", email);
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        removeOne(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<User> set(@NotNull Collection<User> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            clear();
            for (@NotNull final User user : models) {
                repository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserModelRepository repository = new UserRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

}
