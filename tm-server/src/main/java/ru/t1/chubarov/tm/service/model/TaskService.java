package ru.t1.chubarov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.model.ITaskModelRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.model.ITaskService;
import ru.t1.chubarov.tm.api.service.model.IUserService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.UserIdEmptyException;
import ru.t1.chubarov.tm.exception.user.UserNotFoundException;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    private final IUserService userService;

    public TaskService(@NotNull IConnectionService connectionService, IUserService userService) {
        this.connectionService = connectionService;
        this.userService = userService;
    }

    @Override
    public void add(@Nullable String userId, @Nullable Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Task task = findOneById(userId, id);
        task.setStatus(status);
        @NotNull final User user = Optional.of(userService.findOneById(userId)).orElseThrow(UserNotFoundException::new);
        task.setUser(user);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            return repository.findAllByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            @Nullable final Task model = repository.findOneById(id);
            if (model == null) throw new TaskNotFoundException();
            return model;
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public void remove(@NotNull final String userId, @Nullable final Task model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task model = new Task();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(){
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            return repository.getSizeByUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final Task task : models) {
                repository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull final Collection<Task> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
            for (@NotNull final Task task : models) {
                repository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskModelRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
